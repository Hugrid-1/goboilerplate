package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/Hugrid-1/goboilerplate/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/goboilerplate/internal/infrastructure/middleware"
	"gitlab.com/Hugrid-1/goboilerplate/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/auth", func(r chi.Router) {
				authController := controllers.Auth
				r.Post("/register", authController.Register)
				r.Post("/login", authController.Login)
				r.Route("/refresh", func(r chi.Router) {
					r.Use(authCheck.CheckRefresh)
					r.Post("/", authController.Refresh)
				})
				r.Post("/verify", authController.Verify)
			})
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
					r.Post("/password_change", userController.ChangePassword)
				})
			})
		})

	})

	return r
}
