package docs

import (
	ucontroller "gitlab.com/Hugrid-1/goboilerplate/pkg/messages/user"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
// 	-Bearer: []
// responses:
// 	200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}

// swagger:route POST /api/1/user/profile/password_change user changePasswordRequest
// Изменение пароля текущего пользователя.
// security:
// 	-Bearer: []
// responses:
// 	200: changePasswordResponse

// swagger:parameters changePasswordRequest
type changePasswordRequest struct {
	// in: body
	Body ucontroller.ChangePasswordRequest
}

// swagger: response changePasswordResponse

type changePasswordResponse struct {
	// in: body
	Body ucontroller.ChangePasswordResponse
}
