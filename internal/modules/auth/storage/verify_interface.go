package storage

import (
	"context"
)

type Verifier interface {
	GetByEmail(ctx context.Context, email, hash string) (EmailVerifyDTO, error)
	GetByUserID(ctx context.Context, userID int) (EmailVerifyDTO, error)
	Verify(ctx context.Context, userID int) error
	VerifyEmail(ctx context.Context, email, hash string) error
	Create(ctx context.Context, email, hash string, userID int) error
}
