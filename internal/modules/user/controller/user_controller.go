package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Hugrid-1/goboilerplate/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/goboilerplate/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/goboilerplate/internal/infrastructure/handlers"
	"gitlab.com/Hugrid-1/goboilerplate/internal/infrastructure/responder"
	"gitlab.com/Hugrid-1/goboilerplate/internal/modules/user/service"
	"gitlab.com/Hugrid-1/goboilerplate/pkg/messages/user"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, user.ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: user.Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, user.ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: user.Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var changePasswordRequest user.ChangePasswordRequest
	err := u.Decode(r.Body, &changePasswordRequest)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	out := u.service.ChangePassword(r.Context(), service.ChangePasswordIn{
		UserID:      claims.ID,
		OldPassword: changePasswordRequest.OldPassword,
		NewPassword: changePasswordRequest.NewPassword,
	})

	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, user.ChangePasswordResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data:      user.Data{Message: "password change error"},
		})
		return
	}

	u.OutputJSON(w, user.ChangePasswordResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data:      user.Data{Message: "password successfully changed"},
	})

}
