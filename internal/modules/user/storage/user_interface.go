package storage

import (
	"context"
	"gitlab.com/Hugrid-1/goboilerplate/internal/db/adapter"
)

type Userer interface {
	Create(ctx context.Context, u UserDTO) (int, error)
	Update(ctx context.Context, u UserDTO) error
	GetByID(ctx context.Context, userID int) (UserDTO, error)
	GetByIDs(ctx context.Context, ids []int) ([]UserDTO, error)
	GetByEmail(ctx context.Context, email string) (UserDTO, error)
	GetByFilter(ctx context.Context, condition adapter.Condition) ([]UserDTO, error)
}
